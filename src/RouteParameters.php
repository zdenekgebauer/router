<?php

declare(strict_types=1);

namespace ZdenekGebauer\Router;

use function is_string;

class RouteParameters
{
    private static string $regexpParameters = '`(/|\.|)\[([^:\]]*+)(?::([^:\]]*+))?\](\?|)`';

    /**
     * @var array<string>
     */
    private array $parameters = [];

    /**
     * @var array<string>
     */
    private array $prefixes = [
        'i' => '[\d]++', //integer
        '' => '[^/\.]++',
    ];

    public function __construct(private readonly string $uri)
    {
    }

    public function addPrefixRegex(string $prefix, string $regex): void
    {
        $this->prefixes[$prefix] = $regex;
    }

    /**
     * @param array<int|string> $params
     */
    public function createUri(string $uri, array $params = []): string
    {
        $matches = [];
        if (preg_match_all(self::$regexpParameters, $uri, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                [$block, $pre, $type, $param] = $match;
                $block = ($pre !== '' && $pre !== '0' ? substr($block, 1) : $block);
                $replacement = $params[$param] ?? '';
                $uri = str_replace($block, rawurlencode((string)$replacement), $uri);
            }
        }
        return rtrim($uri, '/');
    }

    public function getParameter(string $name): ?string
    {
        return $this->parameters[$name] ?? null;
    }

    /**
     * @return array<string>
     */
    public function getParameters(): array
    {
        return array_map('urldecode', $this->parameters);
    }

    public function match(string $uri): bool
    {
        $regex = $this->prepareRegex($this->uri);
        $uriParameters = [];
        if (preg_match($regex, $uri, $uriParameters) === 1) {
            $this->parameters = array_filter($uriParameters, static fn($key) => is_string($key), ARRAY_FILTER_USE_KEY);
            return true;
        }
        return false;
    }

    private function prepareRegex(string $route): string
    {
        $matches = [];
        if (preg_match_all(self::$regexpParameters, $route, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                [$block, $pre, $type, $param, $optional] = $match;
                $type = $this->prefixes[$type] ?? $type;
                $pre = ($pre === '.' ? '\.' : $pre);
                $optional = ($optional !== '' ? '?' : '');

                $pattern = '(?:' . $pre
                    . '(' . ($param !== '' ? "?<$param>" : '') . $type
                    . ')' . $optional . ')' . $optional;
                $route = str_replace($block, $pattern, $route);
            }
        }
        return "`^$route$`u";
    }
}
