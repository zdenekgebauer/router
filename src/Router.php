<?php

declare(strict_types=1);

namespace ZdenekGebauer\Router;

use InvalidArgumentException;
use LogicException;

use function strlen;

class Router
{
    /**
     * @var array<Route>
     */
    protected array $routes = [];

    /**
     * @var array<Route>
     */
    protected array $namedRoutes = [];

    protected string $basePath = '';

    /**
     * @param string $basePath leading part of the url (/sub/directory/)
     */
    public function __construct(string $basePath = '')
    {
        $this->basePath = rtrim($basePath, '/');
    }

    /**
     * create relative url for a named route with supplied parameters and base path
     *
     * @param array<string, int|string> $parameters associative array of parameters
     */
    public function createUrl(string $routeName, array $parameters = []): string
    {
        if (!isset($this->namedRoutes[$routeName])) {
            throw new InvalidArgumentException('Named route "' . $routeName . '" not found');
        }
        return $this->basePath . $this->namedRoutes[$routeName]->createUri($parameters);
    }

    /**
     * Match a given uri against stored routes. Returns matched route, null on no match.
     */
    public function match(string $uri, string $method = Route::GET): ?Route
    {
        $uri = substr($uri, strlen($this->basePath));
        $parts = explode('?', $uri);
        $uri = $parts[0];
        foreach ($this->routes as $route) {
            if ($route->match($uri, $method)) {
                return $route;
            }
        }
        return null;
    }

    public function replaceRoute(Route $route): void
    {
        unset($this->routes[$route->getUri()], $this->namedRoutes[$route->getName()]);
        $this->addRoute($route);
    }

    public function addRoute(Route $route): void
    {
        if (isset($this->routes[$route->getUri()])) {
            throw new LogicException('Route for "' . $route->getUri() . '" already exists');
        }
        $this->routes[$route->getUri()] = $route;
        if ($route->getName() !== '') {
            if (isset($this->namedRoutes[$route->getName()])) {
                throw new LogicException('Named route "' . $route->getName() . '" already exists');
            }
            $this->namedRoutes[$route->getName()] = $route;
        }
    }
}
