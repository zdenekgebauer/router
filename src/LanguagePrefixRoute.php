<?php

declare(strict_types=1);

namespace ZdenekGebauer\Router;

use InvalidArgumentException;

use function in_array;

/**
 * route prefixed with language, ie '/en/page...', '/cs/page...',
 */
class LanguagePrefixRoute extends Route
{
    final public const PARAMETER_LANGUAGE = 'language';

    protected string $defaultLanguage = '';

    /**
     * @var array<string>
     */
    protected array $allowedLanguages;

    /**
     * $prefixes should contain language codes ['en', 'cs', ...]
     * first element is default language, can be empty string
     *
     * @param array<string> $prefixes
     * @param string|array<Route::*> $method
     */
    public function __construct(array $prefixes, string $uri, $method, callable $handler, string $name = '')
    {
        $prefixes = array_filter($prefixes);
        if ($prefixes === []) {
            throw new InvalidArgumentException('Prefixes are mandatory');
        }
        $this->defaultLanguage = $prefixes[0];
        $this->allowedLanguages = $prefixes;

        $uri = '/[lang:' . self::PARAMETER_LANGUAGE . ']?' . $uri;
        parent::__construct($uri, $method, $handler, $name);
    }

    /**
     * @param array<string> $params
     */
    public function createUri(array $params = []): string
    {
        $params[self::PARAMETER_LANGUAGE] ??= (string)$this->getParameter(self::PARAMETER_LANGUAGE);
        if (
            $params[self::PARAMETER_LANGUAGE] !== ''
            && !in_array($params[self::PARAMETER_LANGUAGE], $this->allowedLanguages, true)
        ) {
            throw new InvalidArgumentException('Disallowed language: ' . $params[self::PARAMETER_LANGUAGE]);
        }
        $uri = parent::createUri($params);
        return str_replace('//', '/', $uri);
    }
}
