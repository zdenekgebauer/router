<?php

declare(strict_types=1);

namespace ZdenekGebauer\Router;

use BadMethodCallException;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

use function in_array;
use function is_array;

class Route
{
    final public const GET = 'GET',
        POST = 'POST',
        PUT = 'PUT',
        HEAD = 'HEAD',
        PATCH = 'PATCH',
        DELETE = 'DELETE',
        DEFAULT_ROUTE_URI = '*';

    /**
     * @var array<string>
     */
    private readonly array $methods;

    private readonly string $uri;

    /**
     * @var callable
     */
    private $handler;

    private readonly RouteParameters $routeParameters;

    /**
     * examples of $uri:
     * '*' route match all urls
     * '/' match root url
     * '/edit/[i:id]' match /edit/12 with integer as $id
     * '/[:controller]/[:action]' match /any/any with string as $controller, $action
     * '/list[i:page]?' match /list/12 with optional integer as $page
     *
     * @param array<string>|string $method
     */
    public function __construct(
        string $uri,
        array|string $method,
        callable $handler,
        private readonly string $name = ''
    ) {
        $methods = is_array($method) ? $method : [trim($method)];
        $this->methods = array_filter(array_map('trim', $methods === [] ? [self::GET] : $methods));
        $uri = trim($uri);
        if ($uri !== self::DEFAULT_ROUTE_URI && $uri[0] !== '/') {
            throw new InvalidArgumentException('URI have to start with slash: ' . $uri);
        }
        $this->uri = $uri;
        $this->handler = $handler;
        $this->routeParameters = new RouteParameters($uri);
    }

    public function addPrefixRegex(string $prefix, string $regex): void
    {
        $this->routeParameters->addPrefixRegex($prefix, $regex);
    }

    /**
     * @param array<int|string> $params
     */
    public function createUri(array $params = []): string
    {
        if ($this->uri === self::DEFAULT_ROUTE_URI) {
            throw new BadMethodCallException('Cannot create uri for default route');
        }
        if (!str_contains($this->uri, '[')) {
            return $this->uri;
        }
        return $this->routeParameters->createUri($this->uri, $params);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParameter(string $parameterName): ?string
    {
        return $this->routeParameters->getParameter($parameterName);
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function invokeHandler(): ResponseInterface
    {
        return $this->getHandler()(...array_values($this->routeParameters->getParameters()));
    }

    public function getHandler(): callable
    {
        return $this->handler;
    }

    public function match(string $uri, string $method): bool
    {
        if ($this->uri === self::DEFAULT_ROUTE_URI) {
            return true;
        }

        if (!in_array($method, $this->methods, true)) {
            return false;
        }

        if ($uri === $this->uri && !str_contains($this->uri, '[')) {
            return true;
        }

        return $this->routeParameters->match($uri);
    }
}
