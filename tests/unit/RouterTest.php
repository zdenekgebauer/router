<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use InvalidArgumentException;
use LogicException;
use Tests\Support\UnitTester;
use ZdenekGebauer\Router\Route;
use ZdenekGebauer\Router\Router;

class RouterTest extends Unit
{
    protected UnitTester $tester;

    public function testAddRouteDuplicate(): void
    {
        $router = new Router();
        $route = new Route(
            '/custom', Route::GET, static function () {
        },  'custom-route'
        );
        $route2 = new Route(
            '/custom2', Route::GET, static function () {
        },  'custom-route'
        );

        $router->addRoute($route);

        $this->tester->expectThrowable(
            new LogicException('Route for "/custom" already exists'),
            static function () use ($router, $route) {
                $router->addRoute($route);
            }
        );

        $this->tester->expectThrowable(
            new LogicException('Named route "custom-route" already exists'),
            static function () use ($router, $route2) {
                $router->addRoute($route2);
            }
        );
    }

    public function testCreateUrlWithInvalidRoute(): void
    {
        $router = new Router();
        $this->tester->expectThrowable(
            new InvalidArgumentException('Named route "invalid-route" not found'),
            static function () use ($router) {
                $router->createUrl('invalid-route');
            }
        );
    }

    public function testMatch(): void
    {
        $router = new Router();
        $handler = static function () {
        };
        $defaultRoute = new Route('*', [], $handler, 'default_route');
        $router->addRoute($defaultRoute);

        $match = $router->match('anything');
        $this->tester->assertEquals($defaultRoute, $match);
        $match = $router->match('anything', 'anything');
        $this->tester->assertEquals($defaultRoute, $match);
    }

    public function testMatchRouteByString(): void
    {
        $router = new Router();
        $handler = static function () {
        };

        $defaultRouter = new Route('/', [Route::GET, Route::POST], $handler, 'default_route');
        $listRoute = new Route('/list', Route::GET, $handler, 'list_route');

        $router->addRoute($defaultRouter);
        $router->addRoute($listRoute);

        $match = $router->match('/');
        $this->tester->assertEquals($defaultRouter, $match);
        $match = $router->match('/', 'POST');
        $this->tester->assertEquals($defaultRouter, $match);

        $this->tester->assertNull($router->match('/', 'PUT'));
        $this->tester->assertEquals($listRoute, $router->match('/list'));
        $this->tester->assertNull($router->match('/list', Route::POST));

        $this->tester->assertEquals('/', $router->createUrl('default_route'));
        $this->tester->assertEquals('/list', $router->createUrl('list_route'));
    }

    public function testMatchRouteWithParameters(): void
    {
        $router = new Router();
        $handler = static function () {
        };

        $editRoute = new Route('/edit/[i:id]', Route::GET, $handler, 'edit_route');

        $router->addRoute($editRoute);

        $match = $router->match('/edit/123');
        $this->tester->assertEquals($editRoute, $match);
        $this->tester->assertEquals(123, $match->getParameter('id'));

        $this->tester->assertNull($router->match('/edit/123', 'PUT'));

        $this->tester->assertEquals('/edit/100', $router->createUrl('edit_route', ['id' => 100]));
    }

    public function testReplaceRoute(): void
    {
        $router = new Router();
        $route = new Route(
            '/custom', Route::GET, static function () {
        },  'custom-route'
        );
        $route2 = new Route(
            '/custom', Route::GET, static function () {
        },  'custom-route'
        );

        $router->addRoute($route);
        $router->replaceRoute($route2);

        $match = $router->match('/custom');
        $this->tester->assertEquals($route2, $match);
    }

}
