<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Utils;
use InvalidArgumentException;
use LogicException;
use Psr\Http\Message\ResponseInterface;
use Tests\Support\UnitTester;
use ZdenekGebauer\Router\Route;
use ZdenekGebauer\Router\Router;

class RouteTest extends Unit
{

    protected UnitTester $tester;

    public function testConstructWithInvalidUri(): void
    {
        $this->tester->expectThrowable(
            new InvalidArgumentException('URI have to start with slash: without_slash'),
            static function () {
                new Route(
                    'without_slash', [], static function () {
                }
                );
            }
        );
    }

    public function testCreateUriDefault(): void
    {
        $route = new Route(
            Route::DEFAULT_ROUTE_URI, Route::GET, static function () {
        }
        );

        $this->tester->expectThrowable(
            new LogicException('Cannot create uri for default route'),
            static function () use ($route) {
                $route->createUri();
            }
        );
    }

    public function testCreateUriRoot(): void
    {
        $route = new Route(
            '/', Route::GET, static function () {
        }
        );
        $this->tester->assertEquals('/', $route->createUri());
    }

    public function testCreateUriWithOptionalParameter(): void
    {
        $route = new Route(
            '/list/[i:page]?', Route::GET, static function () {
        }
        );
        $this->tester->assertEquals('/list', $route->createUri());
        $this->tester->assertEquals('/list/3', $route->createUri(['page' => 3]));

        $route = new Route(
            '/list/[i:page]?/[s:order]?', Route::GET, static function () {
        }
        );
        $this->tester->assertEquals('/list', $route->createUri());
        $this->tester->assertEquals('/list/3/name', $route->createUri(['page' => 3, 'order' => 'name']));
        $this->tester->assertEquals('/list//name', $route->createUri(['order' => 'name']));
    }

    public function testInvokeHandler(): void
    {
        $output = '';
        $handler = static function ($id, $title) use (&$output) {
            $output = 'id:' . $id . ', title:' . $title;
            return new Response(200, ['Content-Type' => 'text/plain'], Utils::streamFor($output));
        };

        $router = new Router();
        $route = new Route('/post/[i:id]/[:title]', Route::GET, $handler);
        $router->addRoute($route);
        $match = $router->match('/post/123/lorem-ipsum');
        $this->tester->assertEquals($route, $match);

        $response = $match->invokeHandler();
        $this->tester->assertInstanceOf(ResponseInterface::class, $response);
        $output = $response->getBody()->__toString();
        $this->tester->assertEquals('id:123, title:lorem-ipsum', $output);
    }

    public function testMatchWitchCustomPrefix(): void
    {
        $router = new Router();
        $route = new Route(
            '/post/[date:from]', [], static function () {
        }
        );
        $route->addPrefixRegex('date', '(\d{4})-(\d{2})-(\d{2})');
        $router->addRoute($route);

        $match = $router->match('/post/2019-08-03');
        $this->tester->assertEquals($route, $match);
        $this->tester->assertEquals('2019-08-03', $match->getParameter('from'));
    }

    public function testMatchWitchCustomPrefixAsFirstParameter(): void
    {
        $router = new Router();
        $route = new Route(
            '/[lang:language]/[:title]?', [], static function () {
        }
        );
        $route->addPrefixRegex('lang', '(cs|en|de)');
        $router->addRoute($route);

        $match = $router->match('/cs/czech-title');
        $this->tester->assertEquals($route, $match);
        $this->tester->assertEquals('cs', $match->getParameter('language'));
        $this->tester->assertEquals('czech-title', $match->getParameter('title'));

        $match = $router->match('/en');
        $this->tester->assertEquals($route, $match);
        $this->tester->assertEquals('en', $match->getParameter('language'));
        $this->tester->assertEquals('', $match->getParameter('title'));

        $match = $router->match('/xx');
        $this->tester->assertNull($match);
    }
}
