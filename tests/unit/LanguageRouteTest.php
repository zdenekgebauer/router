<?php

declare(strict_types=1);

namespace Tests\Unit;

use Codeception\Test\Unit;
use InvalidArgumentException;
use Tests\Support\UnitTester;
use ZdenekGebauer\Router\LanguagePrefixRoute;
use ZdenekGebauer\Router\Route;

class LanguageRouteTest extends Unit
{

    protected UnitTester $tester;

    public function testConstructWithoutPrefixes(): void
    {
        $this->tester->expectThrowable(
            new InvalidArgumentException('Prefixes are mandatory'),
            static function () {
                new LanguagePrefixRoute(
                    ['', ''], '/', Route::GET, static function () {
                }
                );
            }
        );
    }

    public function testCreateUri(): void
    {
        $route = new LanguagePrefixRoute(
            ['en', 'cs'], '/article', Route::GET, static function () {
        }
        );

        $this->tester->assertEquals('/article', $route->createUri());
        $this->tester->assertEquals(
            '/cs/article',
            $route->createUri([LanguagePrefixRoute::PARAMETER_LANGUAGE => 'cs'])
        );
    }

    public function testCreateUriInvalidLanguage(): void
    {
        $route = new LanguagePrefixRoute(
            ['en', 'cs'], '/article', Route::GET, static function () {
        }
        );

        $this->tester->expectThrowable(
            new InvalidArgumentException('Disallowed language: invalid'),
            static function () use ($route) {
                $route->createUri([LanguagePrefixRoute::PARAMETER_LANGUAGE => 'invalid']);
            }
        );
    }

    public function testCreateUriRoot(): void
    {
        $route = new LanguagePrefixRoute(
            ['en', 'cs'], '/', Route::GET, static function () {
        }
        );
        $this->tester->assertEquals('/en', $route->createUri([LanguagePrefixRoute::PARAMETER_LANGUAGE => 'en']));
        $this->tester->assertEquals('/cs', $route->createUri([LanguagePrefixRoute::PARAMETER_LANGUAGE => 'cs']));
    }
}
