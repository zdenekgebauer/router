# Router
Simple url router and generator

## Basic usage

### Create instance of router
```php 
$router = new Router(); 
// if application is not located in root, pass path to application 
$router = new Router('/some/folder');
```

### Register route handlers

* static function  
```php
$router->addRoute(new Route('/static-function', [], static function () {
    echo 'STATIC FUNCTION';
}));
```

* closure 
```php
$myFunction = function () {
    echo 'FUNCTION';
};
$router->addRoute(new Route('/function', [], $myFunction));
```
* method of object 
```php
$controller = new class
{
    public function action()
    {
        echo 'ACTION';
    }
};

$router->addRoute(new Route('/controller-action', [], [$controller, 'action']));

```
* method of class
```php
class StaticController
{
    public static function action()
    {
        echo 'STATIC ACTION';
    }
}
$router->addRoute(new Route('/static-action', [], [StaticController::class, 'action']));
```

### Invoke handler
```php
$match = $this->router->match($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
if ($match) {  
    $match->invokeHandler();
}
echo 'match not found'; // ie page 404  
```

### Routes with parameters
Route can contain variables enclosed in []. Prefix 'i:' (integer) allows only numbers. Suffix '?' marks variable as optional       
```php
// GET /post/123/
// GET /post/123/title
$router->addRoute(new Route('/post/[i:id]/[slug]?', [], static function($id, $slug = null) {
    echo 'id:', $id, "\n",
        'slug:', $slug, "\n";
}));
// get value of variable 
$match = $this->router->match($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
if ($match) {  
    $match->getParameter('id');
    $match->getParameter('slug');
}

```
Another custom prefix
```php
// GET /post/2019-08-10
$route = new Route('/post/[date:date]', [], static function($date) {
    echo $date, "\n";
});
$route->addPrefixRegex('date', '(\d{4})-(\d{2})-(\d{2})');
$router->addRoute($route);
```

### Route for different HTTP method
Pass allowed HTTP methods as second parameter of route. Allowed to are combinations of GET POST, PUT PATCH, HEAD, DELETE 
```php
$router->addRoute(new Route('/route', [], static function(){})); // same as GET /route  
$router->addRoute(new Route('/route', [Route::POST], static function(){})); // POST /route
$router->addRoute(new Route('/route', [Route::POST, Route::PUT], static function(){})); // POST /route or PUT /route
```

### Create url by route
Pass name of route as 4th parameter  
```php 
$router->addRoute(new Route('/post/[i:id]/[s:slug]?', [Route::GET], function($id, $slug = null) {    
}, 'post_route'));
```
Then create url with parameters
```php
echo $router->createUrl('post_route', ['id' => 123, 'slug' => 'title']);  
```

### Default route
One handler for all routes
```php
$router = new Router();
$router->addRoute(new Route(Route::DEFAULT_ROUTE_URI, [], static function () {
    echo 'HANDLER';
}));
```
